import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by lifaxin on 16/7/18.
 */
public class io008_server {

    public static void main(String[] args) {
        try {
            // 在5241端口上建立ServerSocket
            ServerSocket serverSocket = new ServerSocket(5241);
            // 轮询接受网路消息
            while (true) {
                System.out.println("holding on line...");
                // 接受TCP请求
                Socket socket = serverSocket.accept();
                InputStream is = socket.getInputStream();
                byte[] buffer = new byte[1024];
                int readLen = 0;
                int readCount = 0;
                String result = "";
                while ((readLen = is.read(buffer)) != -1) {
                    result += new String(buffer, readCount, readLen);
                    readCount += readLen - 1;
                }
                System.out.println("服务器收到消息: " + result);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
