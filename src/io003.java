import java.io.*;

public class io003 {
    public static void main(String[] args) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File("a.txt")));
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File("d.txt")));
            String temp = null;
            while ((temp = br.readLine()) != null) {
                bw.write(temp);
            }
            br.close();
            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
