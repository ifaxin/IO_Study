import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

/**
 * Created by lifaxin on 16/7/15.
 */
public class io006 {

    public static void main(String[] args) {
        try {
            // 定义管道流
            PipedOutputStream pos = new PipedOutputStream();
            PipedInputStream pis = new PipedInputStream();
            // 链接管道流
            pis.connect(pos);
            // 定义输出线程
            Thread inputThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        pos.write(("W:当前写入数据").getBytes());
                        Thread.sleep(5000);
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            inputThread.start();

            // 定义输入线程
            Thread outputThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    byte[] buffer = new byte[1024];
                    int readLen = 0;
                    try {
                        while (true) {
                            readLen = pis.read(buffer);
                            String s = new String(buffer, 0, readLen);
                            System.out.println(s);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            outputThread.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
