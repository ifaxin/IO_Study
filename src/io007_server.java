import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

/**
 * Created by lifaxin on 16/7/18.
 */
public class io007_server {

    public static void main(String[] args) {
        try {
            // 新建一个UDP的socket
            DatagramSocket udpSocket = new DatagramSocket(5555, InetAddress.getLocalHost());
            // 定义个接受信息的报文类
            byte[] buffer = new byte[1024];
            DatagramPacket datagramPacket = new DatagramPacket(buffer, buffer.length);
            // 轮询IP端口 ,如果获取信息,则输出
            while (true) {
                udpSocket.receive(datagramPacket);
                System.out.println(new String(datagramPacket.getData(), 0, datagramPacket.getLength()));
                datagramPacket.setData(new String("服务器已经收到请求").getBytes());
                udpSocket.send(datagramPacket);
            }
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

