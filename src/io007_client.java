import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Scanner;

/**
 * Created by lifaxin on 16/7/18.
 */
public class io007_client {

    public static void main(String[] args) {
        try {
            // 新建一个UDP的socket
            DatagramSocket udpSocket = new DatagramSocket(5556, InetAddress.getLocalHost());
            byte[] buffer = new byte[1024];
            DatagramPacket datagramPacket = null;
            while (true) {
                System.out.println("请输入发往服务器端的话:");
                Scanner scan = new Scanner(System.in);
                String ask = scan.nextLine();
                System.out.println("您输入了:" + ask);
                datagramPacket = new DatagramPacket(ask.getBytes(), ask.getBytes().length, InetAddress.getLocalHost(), 5555);
                udpSocket.send(datagramPacket);
            }
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
