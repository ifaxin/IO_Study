import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by lifaxin on 16/7/18.
 */
public class io008_client {


    public static void main(String[] args) {
        try {
            while (true) {
                Socket socket = new Socket(InetAddress.getLocalHost(), 5241);
                OutputStream os = socket.getOutputStream();
                System.out.println("请输入你想对服务器说的话:");
                Scanner scan = new Scanner(System.in);
                String requestStr = scan.nextLine();
                os.write(requestStr.getBytes());
                os.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
