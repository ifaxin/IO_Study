import java.io.*;

/**
 * Created by lifaxin on 16/7/15.
 */
public class io004 {


    public static void main(String[] args) {
        try {
            FileInputStream fis = new FileInputStream(new File("data.txt"));
            FileOutputStream fos = new FileOutputStream(new File("data.txt"));
            DataInputStream dis = new DataInputStream(fis);
            DataOutputStream dos = new DataOutputStream(fos);
            // 注意这里 读入的顺序 读取的顺序也一样
            dos.writeUTF("faxingege");
            dos.writeDouble(1.2222222);
            dos.writeBoolean(false);
            dos.flush();
            System.out.println(dis.readUTF());
            System.out.println(dis.readDouble());
            System.out.println(dis.readBoolean());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
