import java.io.*;

public class io002 {
    public static void main(String[] args) {
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File("a.txt")));
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File("c.txt")));
            char[] buffer = new char[1024];
            int readLen = 0;
            int readCount = 0;
            while ((readLen = br.read(buffer)) != -1) {
                bw.write(buffer, readCount, readLen - 1);
                readCount += readLen;

            }
            br.close();
            bw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
