import java.io.*;

public class io001 {
    public static void main(String[] args) {
        try {
            // 1. 获取文件流
            InputStream is = new FileInputStream(new File("a.txt"));
            // 2. 转化成缓冲输入流
            BufferedInputStream bis = new BufferedInputStream(is);
            // 3. 定义文件输出流
            FileOutputStream fos = new FileOutputStream("b.txt");
            // 4. 定义每次读取的大小
            byte[] buffer = new byte[1024];
            // 5. 定义当前读取的字节数
            int readCount = 0;
            // 6. 定义当前读取的长度
            int readLen = 0;
            // 7. 如果没到文件尾,则一直读
            while ((readLen = bis.read(buffer)) != -1) {
                // 8. 通过输出流将每次输出的数据写入b文件中
                fos.write(buffer, readCount, readLen - 1);
                readCount += readLen;
            }
            // 9. 关闭流
            fos.flush();
            is.close();
            bis.close();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
