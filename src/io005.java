import java.io.*;
import java.util.HashMap;

/**
 * Created by lifaxin on 16/7/15.
 */
public class io005 {

    public static void main(String[] args) {
        HashMap<String, String> staff = new HashMap<String, String>();
        staff.put("haha", "hello");
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File("data.txt")));
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File("data.txt")));
            oos.writeObject(staff);
            oos.flush();
            HashMap<String, String> staff1 = (HashMap<String, String>) ois.readObject();
            System.out.println(staff1.get("haha"));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
